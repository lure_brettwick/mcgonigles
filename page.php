<?php
/**
 * Page Template
 *
 * This template is the default page template. It is used to display content when someone is viewing a
 * singular view of a page ('page' post_type) unless another page template overrules this one.
 * @link http://codex.wordpress.org/Pages
 *
 * @package WooFramework
 * @subpackage Template
 */

get_header();
?>

<!-- page.php -->
	

	<!-- SECONDARY NAV -->
	<!-- The structure for this is in functions.php where the widget is registered... -->
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Secondary Menus") ) : ?><?php endif; ?>	

	<!-- SLIDER -->    
    <!-- <div id="slider" class="slider-secondary">
        <?php // if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Slider") ) : ?><?php // endif; ?>
    </div> -->
    <!-- // SLIDER -->

	<!-- FEATURED IMAGE -->    
    <?php if ( has_post_thumbnail() ) : ?>
    	<div id="featured-image">
			<?php the_post_thumbnail(); ?>
		</div>
    <?php endif; ?>    
    <!-- // FEATURED IMAGE -->

    <!-- #content Starts -->
	<?php woo_content_before(); ?>

    <div id="main-container">
    
    	<div id="main-content">    

            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <section id="main" class="col-xs-12">
				<?php if(is_page(44)){ ?>
			 	<div id="fresh-seafood-list">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Fresh Seafood List") ) : ?><?php endif; ?>
				</div><!--end fresh-seafood-list-->
				<div id="seafood-price-link">
				<a href="/kansas-city-seafood-price-chart/">Seafood Price List &amp; Availability</a>
				</div>
			<?php } ?>
				<?php
					woo_loop_before();
					
					if (have_posts()) { $count = 0;
						while (have_posts()) { the_post(); $count++;
							woo_get_template_part( 'content', 'page' ); // Get the page content template file, contextually.
						}
					}
					
					woo_loop_after();
				?>     
            </section><!-- /#main -->
            <?php woo_main_after(); ?>
    
            <?php get_sidebar(); ?>

            

		</div><!-- /#main-sidebar-container -->         

		<?php get_sidebar( 'alt' ); ?>
			
    </div><!-- /#content -->

	<?php woo_content_after(); ?>

<?php get_footer(); ?>