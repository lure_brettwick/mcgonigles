(function($) {
  var cateringInq = function(options) {
    var o = $.extend({}, {
      datepicker: "#catering-date",
      check: "#catering-check",
      result: "#catering-result",
      minDayOut: 1,
    }, options);
    var $datepicker = $(o.datepicker);
    var $check = $(o.check);
    var $result = $(o.result);

		if (!$datepicker.length) {
			return false;
		}

    // initialize datepicker
    $datepicker.datepicker();

    // handle datepicker changes
    function handleChange(event) {
      var today = new Date().setHours(0, 0, 0, 0);
      var limit = today + (1000 * 60 * 60 * 24 * o.minDayOut);
      var datepickerVal = $datepicker.val();
      var selected = new Date(datepickerVal).getTime();
      var pluralize = o.minDayOut > 1 ? "s" : "";

      if (selected >= limit) {
        // catering date is ok, do something
        $result.html('<div class="text-center"><h3>Yes, ' + datepickerVal + ' is available!</h3>Call us to schedule.<br /><br /> <a href="tel:+18164444720">(816) 444-4720</a></div>');
      }
      else {
        // catering date is not ok, do something
        $result.html('<div class="text-center"><h3>Sorry</h3>Please pick a date that is at least ' + o.minDayOut + ' day' + pluralize + ' out.</div>');
      }
    }

    $check.on("click", handleChange);
  };

  $(document).ready(function() {

    cateringInq();

	$('.scroll-to').on('click',function(e) {
	  e.preventDefault();
	  var offset = (47 + 32);
	  var target = this.hash;

	  if ($(this).data('offset') != undefined) offset = $(this).data('offset');

	  $('html, body').stop().animate({
        'scrollTop': $(target).offset().top - offset
	  }, 500, 'swing', function() {
	    // window.location.hash = target;
	  });
    });
  });
})(jQuery);
