<?php
/**
 * Footer Template
 *
 * Here we setup all logic and XHTML that is required for the footer section of all screens.
 *
 * @package WooFramework
 * @subpackage Template
 */

 global $woo_options;

?>
  <div id="footer-top">
    <!-- FOOTER BUCKETS -->
    <div id="section-footer-buckets" class="">
	<?php if(is_page(16)){ ?>
        <div class="container-fluid">
            <div id="footer-buckets-content">
            	<!-- gift ideas-->
                <div id="gift-ideas">
                    <div class="row">
                    	<div class="col-xs-12 col-md-7">
                    		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Top - Gift Ideas") ) : ?><?php endif; ?>
                    	</div>
                    	<div class="hidden-xs hidden-sm col-md-5">
                            <a href="https://www.mcgonigles.com/wp-content/uploads/2016/12/Steak-Buying-Guide.pdf" target="_blank">
                    		  <img src="/wp-content/themes/mcgonigles/img/SteakGuide-Button-H.png" alt="Steak Buying Guide" />
                            </a>
                    	</div>
                    </div>
                </div>
                <!-- // gift ideas -->
                <!-- in-store packages -->
				<div id="in-store-packages">
                    <div class="row">
                    	<div class="col-xs-12 col-md-7">
                    		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Top - In Store Packages") ) : ?><?php endif; ?>
                    	</div>
                    	<div class="hidden-xs hidden-sm col-md-5">
                    		<img src="/wp-content/themes/mcgonigles/img/pic-in-store-packages.jpg" alt="image of In-Store Packages" />
                    	</div>
                    </div>
                </div>
                <!-- // in-store packages -->
            </div>
        </div>
	<?php } ?>
<div class="container-fluid">
            <div id="footer-buckets-content">
            	<!-- steak shipping -->
                <div id="steak-shipping">
                    <div class="row">
                    	<div class="col-xs-12 col-md-6">
                    		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Top - Steak Shipping") ) : ?><?php endif; ?>
                    	</div>
                    	<div class="hidden-xs hidden-sm col-md-6">
                    		<img src="/wp-content/themes/mcgonigles/img/pic-steak-shipping.png" title="Mail Order Steak Shipping of the Finest Cuts of Meat" alt="nation wide steak shipping top quality meat cuts" />
                    	</div>
                    </div>
                </div>
                <!-- // steak shipping -->
                <!-- gift cards -->
				<div id="gift-cards">
                    <div class="row">
                    	<div class="col-xs-12 col-md-6">
                    		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Top - Gift Cards") ) : ?><?php endif; ?>
                    	</div>
                    	<div class="hidden-xs hidden-sm col-md-6">
                    		<img src="/wp-content/themes/mcgonigles/img/pic-gift-cards.jpg" title="A McGonigle's Gift Card Makes the Perfect Gift" alt="mcgonigles market kansas city gift card" />
                    	</div>
                    </div>
                </div>
                <!-- // gift cards -->
            </div>
        </div>
    </div>
    <!-- // FOOTER BUCKETS -->
</div><!--// FOOTER TOP -->
    <!-- FOOTER -->
   <footer id="footer" class="row new">
	<div class="inner">
		<div id="footer-left" class="col-xs-12 col-sm-3">
			<a href="/"><img src="/wp-content/themes/mcgonigles/img/logo.png" title="McGonigle's Market Serving Kansas City Since 1951" alt="MmcGonigles market kansas city local meat market"></a>
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer - Left") ) : ?><?php endif; ?>
		</div><!--end footer-left-->
		<div id="footer-right" class="col-xs-12 col-sm-9">
			<h2>Located at the Corner of Ward Pkwy & 79th St.</h2>
			<div id="map-hours">
				<div id="map" class="col-xs-12 col-sm-8">
					<div class="inner">

					</div><!--end inner-->
				</div><!--end map-->
				<div id="hours" class="col-xs-12 col-sm-4">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer - Hours") ) : ?><?php endif; ?>
				</div><!--end hours-->
			</div><!--end map-hours-->
		</div><!--end footer-right-->
	</div><!--end inner-->
	<div id="overlay-container">
		<div id="footer-social" class="col-xs-12">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer - Social") ) : ?><?php endif; ?>
			</div><!--end footer-social-->
		<div id="copyright" class="col-xs-12 centered">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Menu") ) : ?><?php endif; ?>
        	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Copyright & Bug") ) : ?><?php endif; ?>
		</div><!--end copyright-->
	</div><!--end overlay-container-->
</footer>
    <!-- // FOOTER -->

	<?php wp_footer(); ?>
	<script>
// header: off top scroll behavior
jQuery.noConflict();
jQuery(document).ready(function( $ ) {

	// set variables
	var $document = $(document),
    $element = $('header#header'),
    className = 'offtop';

	$document.scroll(function() {
	  $element.toggleClass(className, $document.scrollTop() >= 50);
	});

});
jQuery.noConflict();
jQuery(document).ready(function( $ ) {

	// set variables
	var $document = $(document),
    $element = $('div#page'),
    className = 'offtop';

	$document.scroll(function() {
	  $element.toggleClass(className, $document.scrollTop() >= 50);
	});

});
</script>

<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/4475707.js"></script>
<!-- End of HubSpot Embed Code -->

<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar("lc_custom_html_bottom")) : ?><?php endif; ?>

</body>
<script language="javascript">
                jQuery( document ).ready( function(){
                    jQuery( "#e_deliverydate" ).attr( "readonly", true );
                    var formats = ["MM d, yy","MM d, yy"];
                    jQuery("#e_deliverydate").val("").datepicker({dateFormat: formats[1], minDate:1, beforeShow: avd, beforeShowDay: chd,
                        onClose:function( dateStr, inst ) {
                            if ( dateStr != "" ) {
                                var monthValue = inst.selectedMonth+1;
                                var dayValue = inst.selectedDay;
                                var yearValue = inst.selectedYear;
                                var all = dayValue + "-" + monthValue + "-" + yearValue;
                                jQuery( "#h_deliverydate" ).val( all );
                            }
                        }
                    });
	               jQuery("#e_deliverydate").parent().append("<small><strong></strong></small>" );
                });
            </script>
</html>
