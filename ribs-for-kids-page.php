<?php
/**
 * Template Name: Ribs For Kids Page
 *
 * Here we setup all logic and XHTML that is required for the index template, used as both the homepage
 * and as a fallback template, if a more appropriate template file doesn't exist for a specific context.
 *
 * @package WooFramework
 * @subpackage Template
 */

get_header();
?>

<!-- page.php -->
	

	<!-- SECONDARY NAV -->
	<!-- The structure for this is in functions.php where the widget is registered... -->
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Secondary Menus") ) : ?><?php endif; ?>	

	<!-- SLIDER -->    
    <!-- <div id="slider" class="slider-secondary">
        <?php // if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Slider") ) : ?><?php // endif; ?>
    </div> -->
    <!-- // SLIDER -->

	<!-- FEATURED IMAGE -->    
    <?php if ( has_post_thumbnail() ) : ?>
    	<div id="featured-image">
			<?php the_post_thumbnail(); ?>
		</div>
    <?php endif; ?>    
    <!-- // FEATURED IMAGE -->

    <!-- #content Starts -->
	<?php woo_content_before(); ?>

    <div id="main-container">
    
    	<div id="main-content">    

            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <section id="main">
				<?php
					woo_loop_before();
					
					if (have_posts()) { $count = 0;
						while (have_posts()) { the_post(); $count++;
							woo_get_template_part( 'content', 'page' ); // Get the page content template file, contextually.
						}
					}
					
					woo_loop_after();
				?>     
            </section><!-- /#main -->
	    
            <section id="rfk-sidebar">
		<?php if(is_page(41)){ ?>
            		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Ribs For Kids Sidebar") ) : ?><?php endif; ?>
		<?php } ?>
		<?php if(is_page(44)){ ?>
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Catch Of The Day") ) : ?><?php endif; ?>
    		<?php } ?>
            </section><!-- /#main -->
            <?php woo_main_after(); ?>
    
            <?php get_sidebar(); ?>

            

		</div><!-- /#main-sidebar-container -->         

		<?php get_sidebar( 'alt' ); ?>

    </div><!-- /#content -->

	<?php woo_content_after(); ?>

<?php get_footer(); ?>