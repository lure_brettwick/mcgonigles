<?php
/**
 * Header Template
 *
 * Here we setup all logic and XHTML that is required for the header section of all screens.
 *
 * @package WooFramework
 * @subpackage Template
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>

		<meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>" />
		<title><?php woo_title(); ?></title>

		<?php woo_meta(); ?>
		<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>" />
		<?php wp_head(); ?>
		<?php woo_head(); ?>

		<!-- google fonts -->
		<link href='//fonts.googleapis.com/css?family=Overlock:700italic|Open+Sans:800italic,700,400|Open+Sans+Condensed:700|Satisfy' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="/wp-content/themes/mcgonigles/fonts/MyFontsWebfontsKit.css"/>
		<!-- font awesome -->
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<?php if(is_page(43) or is_page(44) or is_page(42) or is_page(41) or is_page(47) or is_page(13) or is_page(45) or is_page(46) or is_page(50) or is_page(123) or is_page(125) or is_page(127) or is_page(128) or is_page(18) or is_page(400) or is_page(75) or is_page(433) or is_page(15) or is_page(73) or is_page(467) or is_page(471) or is_page(857)){ ?>
			<style>
			header { background-color: rgb(255, 255, 255);
    			/* RGBa with 0.6 opacity */
   			background-color: rgba(255, 255, 255, 0.6);
			padding: 0 3.5% 0 3.5%;
			}
			h1 {
			font-family: NexaRustScriptB-03 !important;
			margin-bottom: 0;
			padding: 15px 0 5px 0;
			letter-spacing: 1.2px !important;
			}
			#page {background: url(/wp-content/themes/mcgonigles/img/bg-meat-pages.jpg) repeat-y; background-size: 100%;}
			</style>
    		<?php } ?>
<?php if(is_page(433)){ ?>
			<style>
			h1 {
			text-align: center;
			}
			</style>
    		<?php } ?>
		<?php if(is_page(42) or is_page(41) or is_page(47)){ ?>
			<style>
			.meat-description a {color: #008fe1; background: none; padding: initial; font-size: inherit; font-weight: inherit; margin-top: 0; line-height: inherit;}
			.meat-description a:hover {background: none; color: #9d190e;}
			</style>
    		<?php } ?>
		<?php if(is_page(123) or is_page(125) or is_page(127) or is_page(128) or is_page(400) or is_page(857)){ ?>
			<style>
			h1 {text-align: center;}
			</style>
    		<?php } ?>
		<?php if(is_home()){ ?>
			<style>
			#page {background: url(/wp-content/themes/mcgonigles/img/bg-meat-pages.jpg) repeat-y; background-size: 100%;}
			#main-container {background-color: rgb(255, 255, 255);
   			 /* RGBa with 0.6 opacity */
   			 background-color: rgba(255, 255, 255, 0.6);
			 margin-top: 40px;
			}
			#main-container section#main{padding-top: 15px;}
			#main-container section#secondary {padding-top: 0px !important;}
			</style>
		<?php } ?>
		<?php if(is_page(338)){ ?>
			<style>
			#page {background: url(/wp-content/themes/mcgonigles/img/bg-meat-pages.jpg) repeat-y; background-size: 100%;}
			#main-container {background-color: rgb(255, 255, 255);
   			 /* RGBa with 0.6 opacity */
   			 background-color: rgba(255, 255, 255, 0.6);
			}
			#main-container section#main{padding-top: 15px; margin-top: 70px;}
			#main-container section#secondary {padding-top: 0px !important;}
			</style>
		<?php } ?>
<?php if(is_page(805)){ ?>
<style>
#main-container section#main {padding:80px 15px !important;}
.iphorm-group-title {font-size: 20px;}
.iphorm_2_1-input-wrap div.selector {font-size: 16px !important;}
.iphorm_2_1-element-wrap label {font-weight: 700 !important; font-size: 20px !important;}
.iphorm-element-spacer-select label {float: left; width: 90%; display: block; max-width: 490px;}
.iphorm-element-spacer label {font-weight: 400;}
.iphorm-element-wrap .iphorm-input-wrap-select {float-left: width: 10%;display: block;}
.iphorm-group-wrap .iphorm-group-wrap, .iphorm_2_146-group-wrap, .iphorm_2_140-group-wrap, .iphorm_2_57-group-wrap, .iphorm_2_65-group-wrap, .iphorm_2_1-element-wrap {background: #d0d0d0; border-radius: 8px; padding: 10px; max-width: 570px; color: #000 !important;}
.iphorm_2_1-element-wrap {max-width: 680px;}
.iphorm-group-row p {color: #000 !important;}
.iphorm_2_128-outer-label, .iphorm_2_129-outer-label {font-size: 18px !important; font-weight: 700 !important;}
.iphorm_2_162-element-wrap p {font-size: 13px !important; font-weight: 700 !important;}
.iphorm_2_88-group-wrap {width: 100%; max-width: 570px;}
.iphorm_2_88-group-wrap input, .iphorm_2_88-group-wrap textarea {width: 100% !important;}
.tf-label {width: 100%; clear: both; font-weight: 700; overflow: hidden; margin-bottom: 10px;}
.tf-label .label {float: left; width: 90%; display: block; max-width: 490px; font-size: 18px;}
.tf-label .qty {float: left; width: 10%; display: block;}
</style>
<?php } ?>
		<?php if(is_page(16)){ ?>
			<style>
			#main-content {display: none;}
			#section-footer-buckets {background: none;}
			#footer-top {background: url(/wp-content/themes/mcgonigles/img/bg-meat-pages.jpg) repeat-y; background-size: 100%; overflow: hidden;}
			#section-footer-banner {border-bottom: 0; margin-bottom: 30px;}
			</style>
    		<?php } ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-37523351-1', 'auto');
  ga('send', 'pageview');

</script>
<style type="text/css">
			#slider .metaslider .caption-wrap, .meat-description h2, #fresh-seafood-list h2 {font-family:NexaRustScriptB-03 !important;}
			.reset_variations {display: none !important;}
			header#header #user-actions ul {font-size: 15px; padding: 5px 0;}
			.list-inline > li:last-child {padding-right: 0; !important}
			.cart_totals .shipping {display: none;}
			#e_deliverydate_field small:first-of-type {display: none !important;}
			#e_deliverydate_field br:first-of-type {display: none;}
			#e_deliverydate_field small {font-weight: 700;}
			#e_deliverydate_field small br {display: initial !important;}
			#e_deliverydate_field small strong {color: #d11f29;}
			#address_form .h2-link {background-color: #e6e6e6; color: #000; font-weight: 700; font-size: 16px; padding: 3px 10px; margin-top: 10px; display: block; max-width: 244px;}
			#address_form .h2-link:hover {background-color: #da2c35; text-decoration: none; color: #fff;}
 .custom_field_woo input {
background-color:#FFFFFF;
 background-image:none;
  border:1px solid #CCCCCC;
  border-radius:4px;
  box-shadow:rgba(0, 0, 0, 0.0745098) 0 1px 1px inset;
  color:#555555;
  display:block;
  font-size:14px;
  height:34px;
  line-height:1.42857;
  padding:6px 12px;
  transition:border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  width:100%;
}

#ui-datepicker-div {padding: 5px 10px; background: #fff; border: 1px solid #000;}
.ui-datepicker-prev {padding: 0 10px 0 0;}
.green-button {display: block; float: right; background-color: #5cb85c; padding: 7px 15px; color: #fff; clear: both; border-radius: 10px; margin-bottom: 15px;}
.green-button:hover {color: #fff; text-decoration: none;}
input#createaccount {float: left; width: 20px;}
#ship-to-different-address {overflow: hidden; clear: both;}
#ship-to-different-address label {width: auto; float: left; margin: 0 5px 10px 0;}
#ship-to-different-address input {width: 18px; height: 18px; float: left;}
.seafood-list-item #price.table-list-3 {text-align: right;}

@media screen and (max-width: 700px) {
.product_list_widget li {width: 50%;}
.woocommerce .summary table.variations tr td {display: block;}
.create-account {clear: both; overflow: hidden; display: block; float: none;}
.seafood-list-item .table-list-3 {width: 100%;}
.seafood-list-item .table-list-3:last-child {width: 100%;}
}
#main .page-description {overflow: hidden !important;}

@media screen and (max-width: 500px) {
.woocommerce .summary table.variations tr td {display: block;}
.product_list_widget li {width: 100%;}
.woocommerce .summary form {padding: 0 !important; overflow: hidden;}
}

header#header #user-actions ul {width: 180px !important;}
/*
header#header #user-actions li {width: 100%;}
*/
.login-page .table-list-3 {padding: 0 3% 15px 3%;}
.login-page .gray {background-color: #eeeeee;}
.login-page h2 {font-size: 36px; text-align: center;}
.login-page h3 {text-align: center; color: #000; font-size: 28px;}
		</style>

<script type='text/javascript'>
window.__lo_site_id = 76007;

	(function() {
		var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
		wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
	  })();
	</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '228530874239342'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=228530874239342&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

	</head>
	<body <?php body_class(); ?>>
	<?php woo_top(); ?>

	<!-- HEADER -->
	<?php // woo_header_before(); ?>

	<header id="header" class="new-header">
		<div id="header-container">
			<div id="header-content">

				<!-- logo - left column -->
				<div id="logo">
					<a href="https://www.mcgonigles.com/"><img src="/wp-content/themes/mcgonigles/img/logo.png" alt="mcgonigles market local kansas city meat market since 1951" title="McGonigle's Market a Kansas City Tradition Since 1951"></a>
				</div>
				<div id="mobile-headline" class="hidden-xs">
					<span>Must Be<br/>McGonigle's...</span>
					<p>A Kansas City Tradition Since 1951</p>
				</div><!--end mobile-headline-->
				<!-- nav - right column -->
				<div id="nav-primary">
					<div id="header-phone">
						Store Phone:<br/> <strong>(816) 444-4720</strong><br/>
						Shipping Phone:<br/> <strong>1 (888) 783-2540</strong>
					</div><!--end header-phone-->
					<!-- row: sign in -->
					<div id="user-actions-content">
						<div id="user-actions">
							<ul class="list-inline">
    						<li>
									<a class="header-cart-wrapper" href="/cart/">
										<i class="fa fa-shopping-cart" aria-hidden="true"></i>
										<span class="hidden header-cart-count">
											<?php echo WC()->cart->get_cart_contents_count(); ?>
										</span>
									</a>
								</li>
							  <li><a href="/login-page/">Customer Login</a></li>
							</ul>
							<?php // if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("User Actions") ) : ?><?php // endif; ?>
						</div>
					</div>

					<!-- row: nav -->
					<div id="nav-content">

						<nav class="navbar navbar-default">

							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar">Menu</span>
								</button>
								<a class="location" href="https://www.google.com/maps/place/McGonigle's+Market/@38.9852554,-94.6080492,17z/data=!3m1!4b1!4m5!3m4!1s0x87c0eed5d13b2697:0x9532afc7d4596a32!8m2!3d38.9852513!4d-94.6058605" target="_blank">Location</a>
								<a class="header-cart-wrapper" href="/cart/">
									<i class="fa fa-shopping-cart" aria-hidden="true"></i>
									<span class="header-cart-count">
										<?php echo WC()->cart->get_cart_contents_count(); ?>
									</span>
								</a>
								<a class="call-now" href="tel:816-444-4720" target="_blank"><img src="/wp-content/themes/mcgonigles/img/call-now.png" /></a>
							</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<?php wp_nav_menu( array('menu' => 'Primary Navigation' )); ?>
							</div><!-- /.navbar-collapse -->

						</nav>

					</div>

					<!-- row: tagline -->
					<div id="tagline-content" class="hidden-xs">

						<h3 id="tagline" class="new">For Quality Steaks and More, It Must Be McGonigle's...A Kansas City Tradition Since 1951</h3>

					</div>
				</div>
			</div>
		</div>
	</header>

	<?php // woo_header_after(); ?>
	<!-- // HEADER -->

	<!-- SHOPPING PAGES ONLY: FEATURED IMAGE -->
    <div id="shopping-featured-image">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Woocommerce Feature Images") ) : ?><?php endif; ?>
    </div>
    <!-- // SHOPPING PAGES ONLY: FEATURED IMAGE -->

	<!-- CART -->
    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Cart") ) : ?><?php endif; ?>
    <!-- // CART -->

	<div id="page">
