<?php
/**
 * Template Name: Blog
 *
 * The blog page template displays the "blog-style" template on a sub-page.
 *
 * @package WooFramework
 * @subpackage Template
 */

 get_header();
 global $woo_options;
?>

    <!-- template-blog.php -->

    <!-- SECONDARY NAV -->
    <!-- The structure for this is in functions.php where the widget is registered... -->
    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Secondary Menus") ) : ?><?php endif; ?> 

    <!-- SLIDER -->    
    <div id="slider" class="slider-secondary">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Slider") ) : ?><?php endif; ?>
    </div>
    <!-- // SLIDER -->


    <!-- #content Starts -->
	<?php woo_content_before(); ?>
    

    	<div id="main-container">

            <div id="main-content">
            
                <!-- #main Starts -->
                <?php woo_main_before(); ?>

                <section id="main">

                <h1 id="title-blog">Mike's Blog</h1>

                <?php get_template_part( 'loop', 'blog' ); ?>

                </section><!-- /#main -->
                <?php woo_main_after(); ?>    

                <section id="secondary">
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Sidebar") ) : ?><?php endif; ?>
                </section>

            </div>

		</div><!-- /#main-container -->	

    
	<?php woo_content_after(); ?>

<?php get_footer(); ?>