<?php

function lc_enqueue_helper($file_path = null) {
	$file_path_uri = get_stylesheet_directory_uri() . '/' . $file_path;
	$file_last_mod = filemtime(get_stylesheet_directory() . '/' . $file_path);
	return array(
		'uri' => $file_path_uri,
		'file_last_mod' => $file_last_mod,
	);
}

// SPEC STYLES
function load_mcgonigles_styles() {
	// wp_register_style('bootstrap.css', get_stylesheet_directory_uri() . '/css/bootstrap.css', array(), '1', 'all' );
    // wp_enqueue_style( 'bootstrap.css');
		$style = lc_enqueue_helper('css/main.css');
    wp_register_style('main.css', $style['uri'], array(), '1.2', 'all' );
    wp_enqueue_style( 'main.css');
    wp_register_style('slick.css', get_stylesheet_directory_uri() . '/css/slick.css', array(), '1', 'all' );
    wp_enqueue_style( 'slick.css');
    wp_register_style('slick-theme.css', get_stylesheet_directory_uri() . '/css/slick-theme.css', array(), '1', 'all' );
    wp_enqueue_style( 'slick-theme.css');
    // wp_enqueue_style( 'stylesheet', get_stylesheet_uri(), array(), '1', 'all' );
}

// SPEC JS
function load_mcgonigles_js()
    {
        wp_enqueue_script('jquery','/wp-includes/js/jquery/jquery.js','','',true);
        wp_enqueue_script('theme-js', get_stylesheet_directory_uri() . '/js/bootstrap.min.js',array( 'jquery' ),$version,true );
        wp_enqueue_script('catering-js', get_stylesheet_directory_uri() . '/js/catering-teaser.js',array( 'jquery' ), '1.0.1', true );
    }

// SPEC JS FOR HOME PAGE ONLY
function load_mcgonigles_home_js() {
if ( is_front_page() ) {
        wp_enqueue_script('slick', get_stylesheet_directory_uri() . '/js/slick.min.js',array( 'jquery' ),$version,true );
        wp_enqueue_script('waypoints', get_stylesheet_directory_uri() . '/js/jquery.waypoints.min.js',array( 'jquery' ),$version,true );
        wp_enqueue_script('app', get_stylesheet_directory_uri() . '/js/app.js',array( 'jquery' ),$version,true );
    }
}

// LOAD EVERYTHING...
add_action('wp_enqueue_scripts', 'load_mcgonigles_styles');
add_action('wp_enqueue_scripts', 'load_mcgonigles_js');
add_action('wp_enqueue_scripts', 'load_mcgonigles_home_js');

// CUSTOM POST EXCERPT LENGTH
function custom_excerpt_length( $length ) {
	return 25;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// CUSTOM NAV WALKER TO BOOTSTRAP NAV
require_once('lib/wp_bootstrap_navwalker.php');


// SPEC SIDEBARS...
// user actions
register_sidebar(
    array(
    'name' => 'User Actions',
    'id' => 'user-actions',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// social - top
register_sidebar(
    array(
    'name' => 'Social - Top',
    'id' => 'social-top',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// cart
register_sidebar(
    array(
    'name' => 'Cart',
    'id' => 'commerce-cart',
    'before_widget' => '<div id="top-cart"><div id="top-cart-container"><div id="top-cart-content"><div class="col-xs-12">',
    'after_widget' => '</div></div></div></div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// slider
register_sidebar(
    array(
    'name' => 'Slider',
    'id' => 'slider',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// feature images on woocommerce pages
register_sidebar(
    array(
    'name' => 'Woocommerce Feature Images',
    'id' => 'woocomm-feature-image',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// woocommerce right sidebar
register_sidebar(
    array(
    'name' => 'Shop Right Column',
    'id' => 'woocomm-shop-right-column',
    'before_widget' => '<div class="shop-right-column">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// home - specials
register_sidebar(
    array(
    'name' => 'Home - Specials',
    'id' => 'home-specials',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// callout - fine meats
register_sidebar(
    array(
    'name' => 'Fine Meats - Callout',
    'id' => 'callout-fine-meats',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// best sellers - scroller
register_sidebar(
    array(
    'name' => 'Best Sellers - Scroller',
    'id' => 'best-sellers-scroller',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// callout - fresh catch
register_sidebar(
    array(
    'name' => 'Fresh Catch - Callout',
    'id' => 'callout-fresh-catch',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// copy - fresh catch
register_sidebar(
    array(
    'name' => 'Fresh Catch - Copy',
    'id' => 'copy-fresh-catch',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// Fresh Seafood List
register_sidebar(
    array(
    'name' => 'Fresh Seafood List',
    'id' => 'fresh-seafood',
    'before_widget' => '',
    'after_widget' => '',
));
// callout - bbq to go
register_sidebar(
    array(
    'name' => 'BBQ - Callout',
    'id' => 'callout-bbq',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// Choice Rewards
register_sidebar(
    array(
    'name' => 'Choice Rewards',
    'id' => 'choice-rewards',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h2>',
    'after_title' => '</h2>',
));
// timeline
register_sidebar(
    array(
    'name' => 'Timeline',
    'id' => 'timeline-widget',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
//Ribs for Kids Sidebar
register_sidebar(
    array(
    'name' => 'Ribs For Kids Sidebar',
    'id' => 'rfk-sidebar',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
//Fresh Catch of The Day Sidebar
register_sidebar(
    array(
    'name' => 'Catch Of The Day',
    'id' => 'catch-of-day',
    'before_widget' => '<div id="%1$s">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// footer top - gift ideas
register_sidebar(
    array(
    'name' => 'Footer Top - Gift Ideas',
    'id' => 'footer-top-gift-ideas',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// footer top - in store packages
register_sidebar(
    array(
    'name' => 'Footer Top - In Store Packages',
    'id' => 'footer-top-packages',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// footer top - steak shipping
register_sidebar(
    array(
    'name' => 'Footer Top - Steak Shipping',
    'id' => 'footer-top-shipping',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// footer top - gift cards
register_sidebar(
    array(
    'name' => 'Footer Top - Gift Cards',
    'id' => 'footer-top-gift-cards',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// footer - banner
register_sidebar(
    array(
    'name' => 'Footer - Banner',
    'id' => 'footer-bnr',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// footer - left
register_sidebar(
    array(
    'name' => 'Footer - Left',
    'id' => 'footer-left',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// footer - right
register_sidebar(
    array(
    'name' => 'Footer - Right',
    'id' => 'footer-right',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// footer - map
register_sidebar(
    array(
    'name' => 'Footer - Map',
    'id' => 'footer-map',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// footer - hours
register_sidebar(
    array(
    'name' => 'Footer - Hours',
    'id' => 'footer-hours',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// footer menu
register_sidebar(
    array(
    'name' => 'Footer Menu',
    'id' => 'footer-menu',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// footer social
register_sidebar(
    array(
    'name' => 'Footer Social',
    'id' => 'footer-social',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// copyright & bug
register_sidebar(
    array(
    'name' => 'Copyright & Bug',
    'id' => 'copyright-bug',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// secondary menu
register_sidebar(
    array(
    'name' => 'Secondary Menus',
    'id' => 'secondary-menu',
    'before_widget' => '
    	<div id="secondary-nav-wrapper" class="hidden-xs">
			<div id="secondary-nav-container">
				<div id="secondary-nav-content">
					<div id="secondary-nav">',
    'after_widget' => '</div></div></div></div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// twitter
register_sidebar(
    array(
    'name' => 'Twitter',
    'id' => 'twitter',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));
// blog sidebar
register_sidebar(
    array(
    'name' => 'Blog Sidebar',
    'id' => 'blog-sidebar',
    'before_widget' => '<div class="widget-sidebar">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));

// Beef Price List
register_sidebar(
    array(
    'name' => 'Beef Price List',
    'id' => 'beef-price-list',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));

// Pork Price List
register_sidebar(
    array(
    'name' => 'Pork Price List',
    'id' => 'pork-price-list',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));

// Lamb & Veal Price List
register_sidebar(
    array(
    'name' => 'Lamb & Veal Price List',
    'id' => 'lamb-price-list',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));

// Poultry Price List
register_sidebar(
    array(
    'name' => 'Poultry Price List',
    'id' => 'poultry-price-list',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));

// Buffalo & Game Price List
register_sidebar(
    array(
    'name' => 'Buffalo & Game Price List',
    'id' => 'buffalo-price-list',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));

// Seafood Price List
register_sidebar(
    array(
    'name' => 'Seafood Price List',
    'id' => 'seafood-price-list',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<h3>',
    'after_title' => '</h3>',
));

// Custon HTML site bottom, use for popups, etc.
register_sidebar(
  array(
    'name' => 'Custom HTML - Site Bottom',
    'description' => 'For best results, use Custom HTML widget.',
    'id' => 'lc_custom_html_bottom',
    'before_widget' => '',
    'after_widget' => '',
    'before_title' => '<!-- ',
    'after_title' => ' -->',
  )
);

register_sidebar(
    array(
      'name' => 'Home Notice',
      'description' => 'Prominent notice on the home page for holiday hours, etc.',
      'id' => 'lc_home_notice',
      'before_widget' => '',
      'after_widget' => '',
      'before_title' => '<!-- ',
      'after_title' => ' -->',
    )
);

/* custom tax: specials and price list category */
function position_init() {
	// create a new taxonomy
	register_taxonomy(
		'specials-category',
		'weekly-specials',
		array(
			'label' => __( 'Specials Category' ),
			'show_ui' => true,
        		'show_admin_column' => true,
			'rewrite' => array( 'slug' => 'specials-category' )
		)
	);
        // create a new taxonomy
	register_taxonomy(
		'beef-categories',
		'beef-price-list',
		array(
		'label' => __( 'Beef Categories' ),
		'show_ui' => true,
        	'show_admin_column' => true,
              	'hierarchical' => true,
			'rewrite' => array( 'slug' => 'beef-categories' )
		)
	);
          // create a new taxonomy
	register_taxonomy(
		'pork-categories',
		'pork-price-list',
		array(
			'label' => __( 'Pork Categories' ),
			'show_ui' => true,
        		'show_admin_column' => true,
                        'hierarchical' => true,
			'rewrite' => array( 'slug' => 'pork-categories' )
		)
	);
          // create a new taxonomy
	register_taxonomy(
		'lamb-categories',
		'lamb-price-list',
		array(
			'label' => __( 'Lamb & Veal Categories' ),
			'show_ui' => true,
        		'show_admin_column' => true,
			'hierarchical' => true,
			'rewrite' => array( 'slug' => 'lamb-categories' )
		)
	);
          // create a new taxonomy
	register_taxonomy(
		'poultry-categories',
		'poultry-price-list',
		array(
			'label' => __( 'Poultry Categories' ),
			'show_ui' => true,
        		'show_admin_column' => true,
			'hierarchical' => true,
			'rewrite' => array( 'slug' => 'poultry-categories' )
		)
	);
          // create a new taxonomy
	register_taxonomy(
		'buffalo-categories',
		'buffalo-price-list',
		array(
			'label' => __( 'Buffalo & Game Categories' ),
			'show_ui' => true,
        		'show_admin_column' => true,
			'hierarchical' => true,
			'rewrite' => array( 'slug' => 'buffalo-categories' )
		)
	);
	  // create a new taxonomy
	register_taxonomy(
		'seafood-categories',
		'seafood-price-list',
		array(
			'label' => __( 'Seafood Categories' ),
			'show_ui' => true,
        		'show_admin_column' => true,
			'hierarchical' => true,
			'rewrite' => array( 'slug' => 'seafood-categories' )
		)
	);
}
add_action( 'init', 'position_init' );

/* CUSTOM POST TYPE: Fresh Seafood */
add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type( 'fresh-seafood',
		array(
			'labels' => array(
				'name' => __( 'Fresh Seafood' ),
				'singular_name' => __( 'Fresh Seafood' )
			),
			'taxonomies' => array('category'),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'fresh-seafood'),
		)
	);
	register_post_type( 'weekly-specials',
		array(
			'labels' => array(
				'name' => __( 'Weekly Specials' ),
				'singular_name' => __( 'Weekly Specials' )
			),
			'taxonomies' => array('specials-category'),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'weekly-specials'),
		)
	);
        register_post_type( 'beef-price-list',
		array(
			'labels' => array(
				'name' => __( 'Beef Price List' ),
				'singular_name' => __( 'Beef Price List' )
			),
			'taxonomies' => array('beef-categories'),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'beef-price-list'),
		)
	);
        register_post_type( 'pork-price-list',
		array(
			'labels' => array(
				'name' => __( 'Pork Price List' ),
				'singular_name' => __( 'Pork Price List' )
			),
			'taxonomies' => array('pork-categories'),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'pork-price-list'),
		)
	);
        register_post_type( 'lamb-price-list',
		array(
			'labels' => array(
				'name' => __( 'Lamb & Veal Price List' ),
				'singular_name' => __( 'Lamb & Veal Price List' )
			),
			'taxonomies' => array('lamb-categories'),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'lamb-price-list'),
		)
	);
        register_post_type( 'poultry-price-list',
		array(
			'labels' => array(
				'name' => __( 'Poultry Price List' ),
				'singular_name' => __( 'Poultry Price List' )
			),
			'taxonomies' => array('poultry-categories'),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'poultry-price-list'),
		)
	);
        register_post_type( 'buffalo-price-list',
		array(
			'labels' => array(
				'name' => __( 'Buffalo & Game Price List' ),
				'singular_name' => __( 'Buffalo & Game Price List' )
			),
			'taxonomies' => array('buffalo-categories'),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'buffalo-price-list'),
		)
	);
	 register_post_type( 'seafood-price-list',
		array(
			'labels' => array(
				'name' => __( 'Seafood Price List' ),
				'singular_name' => __( 'Seafood Price List' )
			),
			'taxonomies' => array('seafood-categories'),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'seafood-price-list'),
		)
	);
}

function myextensionTinyMCE($init) {
    // Command separated string of extended elements
    $ext = 'span[id|name|class|style]';

    // Add to extended_valid_elements if it alreay exists
    if ( isset( $init['extended_valid_elements'] ) ) {
        $init['extended_valid_elements'] .= ',' . $ext;
    } else {
        $init['extended_valid_elements'] = $ext;
    }

    // Super important: return $init!
    return $init;
}

add_filter('tiny_mce_before_init', 'myextensionTinyMCE' );

/*-----------------------------------------------------------------------------------*/
/* You can add custom functions below */
/*-----------------------------------------------------------------------------------*/

add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

	$tabs['description']['title'] = __( 'Cooking' );		// Rename the description tab
	return $tabs;

}


add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}

/**
 * Change on single product panel "Product Description"
 * since it already says "features" on tab.
 */
add_filter('woocommerce_product_description_heading',
'isa_product_description_heading');

function isa_product_description_heading() {
    return __('Cooking Instructions', 'woocommerce');
}


/**
 * Change text strings
 *
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/gettext
 */
function my_text_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'View Cart' :
            $translated_text = __( 'Next Step', 'woocommerce' );
            break;
    }
    return $translated_text;
}
add_filter( 'gettext', 'my_text_strings', 20, 3 );

// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     $fields['shipping']['shipping_phone'] = array(
        'label'     => __('Phone', 'woocommerce'),
    'placeholder'   => _x('Phone', 'placeholder', 'woocommerce'),
    'required'  => true,
    'class'     => array('form-row-wide'),
    'clear'     => true
     );

     return $fields;
}

add_filter('woocommerce_checkout_fields', 'custom_woocommerce_checkout_fields');

function custom_woocommerce_checkout_fields( $fields ) {
     $fields['order']['order_comments']['label'] = 'Enclosure Card';
     $fields['order']['order_comments']['placeholder'] = 'Write Your Message - Include Your Name!';
     $fields['order']['order_comments']['maxlength'] = '150';
     $fields['order']['order_comments']['type'] = 'textarea';
     return $fields;
}


add_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

function custom_pre_get_posts_query( $q ) {

	if ( ! $q->is_main_query() ) return;
	if ( ! $q->is_post_type_archive() ) return;

	if ( ! is_admin() && is_shop() ) {

		$q->set( 'tax_query', array(array(
			'taxonomy' => 'product_cat',
			'field' => 'slug',
			'terms' => array( 'special-packages' ), // Don't display products in the special packages category on the shop page
			'operator' => 'NOT IN'
		)));

	}

	remove_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

}

function woocommerce_button_proceed_to_checkout() {
       $checkout_url = WC()->cart->get_checkout_url();
       ?>
       <a href="/shop/" class="checkout-button button alt wc-forward" style="margin-bottom: 15px !important;"><?php _e( 'Continue Shopping', 'woocommerce' ); ?></a><br />
       <a href="<?php echo $checkout_url; ?>" class="checkout-button button alt wc-forward"><?php _e( 'Check On Out', 'woocommerce' ); ?></a>
       <?php
     }

function oakwood_tinymcefix() {
    wp_enqueue_script( 'tiny_mce' );
    echo '<script type="text/javascript">'
   , 'tinymce.init({
            ...
            extended_valid_elements : "span[*]"
        });'
   , '</script>';
}
add_action( 'admin_enqueue_scripts', 'oakwood_tinymcefix' );

/**
 * Redirect to a specific page when clicking on Continue Shopping in the cart
 *
 * @return void
 */
function wc_custom_redirect_continue_shopping() {
    return '/shop/';
}
add_filter( 'woocommerce_continue_shopping_redirect', 'wc_custom_redirect_continue_shopping' );


function my_customize_autoreply($mailer, $form, $attachments)
{
    ob_start();
    include IPHORM_INCLUDES_DIR . '/emails/email-html-holiday.php';
    $emailHTML = ob_get_clean();

    ob_start();
    include IPHORM_INCLUDES_DIR . '/emails/email-plain.php';
    $emailPlain = ob_get_clean();

    $mailer->MsgHTML($emailHTML);
    $mailer->AltBody = $emailPlain;

    // You must return the $mailer object
    return $mailer;
}
add_filter('iphorm_pre_send_autoreply_email_8', 'my_customize_autoreply', 10, 3);

add_filter('iphorm_element_valid_iphorm_8_171', 'my_confirm_email', 10, 3);

add_filter('iphorm_pre_send_autoreply_email_10', 'my_customize_autoreply', 10, 3);

add_filter('iphorm_element_valid_iphorm_10_275', 'my_confirm_email', 10, 3);

function my_confirm_email($valid, $value, $element)
{
    if ($value != $element->getForm()->getValue('iphorm_10_92')) {
        $valid = false;
        $element->addError('The email addresses do not match');
    }

    return $valid;
}

// Header shopping cart count
function lc_cart_count_fragments($fragments) {
  $fragments['span.header-cart-count'] = '<span class="header-cart-count">' . WC()->cart->get_cart_contents_count() . '</span>';

  return $fragments;
}
add_filter('woocommerce_add_to_cart_fragments', 'lc_cart_count_fragments', 10, 1);
