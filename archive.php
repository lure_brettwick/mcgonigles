<?php
/**
 * Archive Template
 *
 * The archive template is a placeholder for archives that don't have a template file. 
 * Ideally, all archives would be handled by a more appropriate template according to the
 * current page context (for example, `tag.php` for a `post_tag` archive).
 *
 * @package WooFramework
 * @subpackage Template
 */

 global $woo_options;
 get_header();
?>     

    <!-- archive.php -->

    <!-- #content Starts -->
	<?php woo_content_before(); ?>    
    
    	<div id="main-container">    
		
            <div id="main-content">
                
                <!-- #main Starts -->
                <?php woo_main_before(); ?>
                <section id="main">
                    
                <?php get_template_part( 'loop', 'archive' ); ?>
                        
                </section><!-- /#main -->
                <?php woo_main_after(); ?>

                <section id="secondary">
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Sidebar") ) : ?><?php endif; ?>
                </section>

            </div>            
    
		</div><!-- /#main-container -->         
    
	<?php woo_content_after(); ?>
		
<?php get_footer(); ?>