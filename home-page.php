<?php
/**
 * Template Name: Home Page
 *
 * Here we setup all logic and XHTML that is required for the index template, used as both the homepage
 * and as a fallback template, if a more appropriate template file doesn't exist for a specific context.
 *
 * @package WooFramework
 * @subpackage Template
 */

 get_header();
 global $woo_options;
?>
    <?php if (is_active_sidebar('lc_home_notice')) : ?>
        <div class="home-notice-prominent">
            <?php dynamic_sidebar('lc_home_notice'); ?>
        </div>
    <?php endif; ?>

    <!-- SLIDER -->
    <div id="slider" class="hidden-xs">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Slider") ) : ?><?php endif; ?>
    </div>
    <!-- // SLIDER -->
	<!-- Christmas Hours
	<div id="christmas-hours">
		<strong>Holiday Weekend Hours</strong> &nbsp; &nbsp; &nbsp; Fri 12/29 &amp; Sat 12/30: 8am-6pm, New Year's Eve: 11am-5pm, Closed New Year's Day
	</div>
	<style>#christmas-hours {width: 100%; padding: 12px 8px; background:#47901F; color: #fff; font-size: 23px; text-align: center; text-shadow: 0 0 4px #000; line-height: 1.3em;}</style>
end christmas-hours-->
    <!-- FUNNEL -->
    <div id="section-funnel" class="hidden-xs">
        <div id="funnel-container">
            <div id="funnel-content">
              <ul>
								<li id="fnl-market">
									<a class="hvr-float-shadow" href="/local-market-kansas-city/" title="McGonigle's Local Market" alt="local meat market in kansas city">Our Market</a>
								</li>
								<li id="fnl-online">
									<a class="hvr-float-shadow" href="/shop/" title="Shop Online For the Finest Meats" alt="mail order steak shipping">Ship Steaks</a>
								</li>
								<li id="fnl-to-go">
									<a class="hvr-float-shadow" href="/local-market-kansas-city/kansas-city-bbq-equipment-supplies/" title="McGonigle's BBQ" alt="BBQ To Go Kansas city" >BBQ To Go</a>
								</li>
								<li id="fnl-cater">
									<a class="hvr-float-shadow" href="/local-market-kansas-city/catering-kansas-city/" title="Local Kansas City Catering" alt="catering company located in kansas city">Catering</a>
								</li>
				  				<li id="fm-gift-cards">
									<a  class="hvr-float-shadow" href="/gift-ideas/gift-cards/" title="Gift Cards" alt="Gift Cards">Gift Cards</a>
								</li>
								<!--<li id="fnl-holiday">
									<a class="hvr-float-shadow" href="/holiday-order/" title="Holiday Orders" alt="Holiday Orders">
										<span style="font-size: 26px;">Holiday Orders</span>
									</a>
								</li>
							    <li id="fnl-turkey">
									<a class="hvr-float-shadow" href="/turkey-order/" title="Turkey Orders" alt="Turkey Orders">
										<span style="font-size: 26px;">Turkey Orders</span>
									</a>
								</li> -->
							</ul>
            </div>
        </div>
    </div>
    <!-- // FUNNEL -->
	<div id="funnel-mobile">
		<ul>
			<li id="fm-local-market">
				<a href="/local-market-kansas-city/"><span class="sr-only">Local Market</span></a>
			</li>
			<li id="fm-weekly-specials">
				<a href="/weekly-specials/"><span class="sr-only">Weekly Specials</span></a>
			</li>
			<li id="fm-steak-shipping">
				<a href="/shop/"><span class="sr-only">Steak Shipping</span></a>
			</li>
			<li id="fm-bbq">
				<a href="/local-market-kansas-city/kansas-city-bbq-equipment-supplies/"><span class="sr-only">BBQ to Go</span></a>
			</li>
			<li id="fm-catering">
				<a href="/local-market-kansas-city/catering-kansas-city/"><span class="sr-only">Catering</span></a>
			</li>
			<li id="fm-holiday">
				<a href="/holiday-order/"><span class="sr-only">Holiday Orders</span></a>
			</li>
			<!-- <li id="fm-turkey-order-form">
				<a href="/turkey-order/"><span class="sr-only">Turkey Orders</span></a>
			</li> -->
		</ul>
	</div><!--end funnel-mobile-->
    <!-- HOME SPECIALS -->
    <div class="row hidden-xs" id="home-specials">
    	<div class="inner">
    		 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Home - Specials") ) : ?><?php endif; ?>
    	</div><!--end inner-->
    </div><!--end home-specials-->
	<!-- // HOME SPECIALS -->

 <!-- FINE MEETS -->
  <div class="row hidden-xs" id="home-steak-shipping">
  		<div class="inner">
  			<div id="logo-callout" class="col-xs-12 col-sm-4">
                    <img src="/wp-content/themes/mcgonigles/img/logo-fine-meats.png" title="Providing The Finest Meats to Kansas City Since 1951" alt="Fine Meats logo" />
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Fine Meats -  Callout") ) : ?><?php endif; ?>
  			</div><!--end logo-callout-->
  			<div id="steak" class="col-xs-12 col-sm-8">
  				 <img src="/wp-content/themes/mcgonigles/img/steak.png" alt="Steak Image" />
  			</div><!--end steak-->
  		</div><!--end inner-->
  </div><!--end row-->
<!-- // FINE MEETS -->
<!-- FRESH CATCH -->
   <div id="home-fresh-catch" class="row hidden-xs">
	<div class="inner">
		<div class="col-xs-hidden col-sm-3"></div>
		<div class="col-xs-12 col-sm-5" id="fresh-catch-details">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Fresh Catch - Copy") ) : ?><?php endif; ?>
		</div>
		<div class="col-xs-12 col-sm-4" id="fresh-catch-logo">
			 <img src="/wp-content/themes/mcgonigles/img/logo-fresh-catch.png" title="Fresh Catch of the Day" alt="fresh seafood fish local kansas city market" />
             <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Fresh Catch - Callout") ) : ?><?php endif; ?>
		</div>
	</div><!--end inner-->
</div>
<!-- // FRESH CATCH -->

    <!-- BBQ TO GO -->
    <div id="section-bbg-to-go" class="hidden-xs">
        <div id="bbq-to-go-container">
            <div id="bbq-to-go-content">
            	<div id="bbq-to-go-logo">
                    <img src="/wp-content/themes/mcgonigles/img/logo-bbq-to-go.png" title="BBQ To Go at McGonigle's" alt="BBQ To Go kansas city area" />
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("BBQ - Callout") ) : ?><?php endif; ?>
                </div>
                <div id="burger" class="hidden-xs">
                    <img src="/wp-content/themes/mcgonigles/img/pic-burger-ko.png" alt="Burnt End Sandwich" />
                </div>
            </div>
        </div>
    </div>
<!-- // BBQ TO GO -->
<!-- CHOICE REWARDS -->
<div id="choice-rewards-section" class="row hidden-xs">
	<div class="inner">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Choice Rewards") ) : ?><?php endif; ?>
	</div><!--end inner-->
</div><!--end choice-rewards-section-->
<!-- // CHOICE REWARDS -->
    <!-- TIMELINE -->
    <div id="section-timeline" class="hidden-xs">
        <div id="timeline-liner">
            <div id="timeline-container">
                <div id="timeline-content">
                    <div id="timeline">
                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Timeline") ) : ?><?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- // TIMELINE -->
     <!-- MIKE'S BLOG -->
    <div id="section-blog" class="hidden-xs">
        <div id="blog-container">
            <div id="blog-content">
                <div id="blog-items">
                    <h2>Mike's Blog</h2>
                    <img class="hidden-md hidden-lg" src="/wp-content/themes/mcgonigles/img/logo-mikes-blog.png" title="Check Out Mike's Blog for the Latest News, Events and Special Offers" alt="Mike's Blog news tips specials" />
                    <ul>
                        <?php $the_query = new WP_Query( 'showposts=3' ); ?>

                        <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
                        <li>

                            <!-- month / day utilities, in case you need them -->
                            <!-- <div class="month"><?php // echo mysql2date('M', $post->post_date) ?></div> -->
                            <!-- <div class="day"><?php // echo mysql2date('d', $post->post_date) ?></div> -->


                            <div class="media">
                                <?php if ( has_post_thumbnail() ): ?>
                                <div class="media-left">
                                    <a href="<?php the_permalink() ?>">
                                       <?php the_post_thumbnail( 'thumbnail' ); ?>
                                    </a>
                                </div>
                                <?php endif; ?>
                                <div class="media-body">
                                    <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                                    <?php the_excerpt(); ?>
                                    <a class="link-read-more" href="<?php the_permalink() ?>">
                                       [ READ MORE ]
                                    </a>
                                </div>
                            </div>

                        </li>
                        <?php endwhile;?>
                    </ul>
                </div>
                <div id="logo-callout" class="hidden-xs hidden-sm">
                    <img src="/wp-content/themes/mcgonigles/img/logo-mikes-blog.png" alt="Mike's Blog logo" />
                </div>
            </div>
        </div>
    </div>
    <!-- // MIKE'S BLOG -->

<?php get_footer(); ?>
