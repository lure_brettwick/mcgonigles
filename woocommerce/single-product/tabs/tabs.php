<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>

	<div id="product-accordion" class="panel-group">
		<?php foreach ( $tabs as $key => $tab ) : ?>
			<div class="panel panel-default">
				<div id="panel-title-<?php echo esc_attr( $key ); ?>" class="panel-heading <?php echo esc_attr( $key ); ?>">
					<h4 class="panel-title">
						<a data-toggle="collapse" href="#product-panel-<?php echo esc_attr( $key ); ?>" data-parent="#product-accordion" class="collapsed">
							<?php
								$heading = esc_html( apply_filters( 'woocommerce_product_description_heading', __( 'Description', 'woocommerce' ) ) );

								if ($heading) {
									echo $heading;
								}
								else {
									echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key );
								}
							?>
							<i class="collapse-indicator">
								<i class="fa fa-chevron-down"></i>
							</i>
						</a>
					</h4>
				</div>
				<div id="product-panel-<?php echo esc_attr( $key ); ?>" class="panel-collapse collapse">
					<div class="panel-body">
						<?php if ( isset( $tab['callback'] ) ) { call_user_func( $tab['callback'], $key, $tab ); } ?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>

<?php endif; ?>
