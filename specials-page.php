<?php
/**
 * Template Name: Specials & Price List
 *
 * Here we setup all logic and XHTML that is required for the index template, used as both the homepage
 * and as a fallback template, if a more appropriate template file doesn't exist for a specific context.
 *
 * @package WooFramework
 * @subpackage Template
 */

get_header();
?>      
<style>
#content-bg {background: url(/wp-content/uploads/2016/04/bg-specials2.jpg) no-repeat; background-size: cover; background-position: center; width: 100%;}
.specials-container article {background: #fff; overflow: hidden; padding: 20px 2%; background-color:rgba(255, 255, 255, 0.8);}
.specials-container h1, .specials-container h2.special-header, .specials-container h3.special-subheader {font-family: NexaRustScriptB-03 !important; text-align: center;}
.specials-container h1 {font-size: 44px !important;}
.specials-container h2 {font-size: 28px;}
.specials-container h2.special-header {font-size: 32px; color: #fff !important; display: block; background: url(/wp-content/uploads/2016/04/specials-header.png) no-repeat; background-size: 100% 100%; padding: 15px 0;}
.specials-container h2.special-header strong {background: #961c1d; padding: 0 15px;}
.specials-container h3 {font-size: 16px;}
.specials-container h3.special-subheader {font-size: 28px; color: #8e2006;}
.specials-container ul li {font-weight: 400;}
.specials-container ul li strong {text-transform: uppercase;}
.specials-container ul li span {font-weight: 700;}
.specials-container .table-list-2 {padding-left: 2%; padding-right: 2%;}
</style>
<!-- page.php -->
	

	<!-- SECONDARY NAV -->
	<!-- The structure for this is in functions.php where the widget is registered... -->
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Secondary Menus") ) : ?><?php endif; ?>	

	<!-- SLIDER -->    
    <!-- <div id="slider" class="slider-secondary">
        <?php // if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Slider") ) : ?><?php // endif; ?>
    </div> -->
    <!-- // SLIDER -->

	<!-- FEATURED IMAGE -->    
    <?php if ( has_post_thumbnail() ) : ?>
    	<div id="featured-image">
			<?php the_post_thumbnail(); ?>
		</div>
    <?php endif; ?>    
    <!-- // FEATURED IMAGE -->

    <!-- #content Starts -->
	<?php woo_content_before(); ?>
    <div id="content-bg">
    <div id="main-container">
    
    	<div id="main-content" class="specials-container">    

            <!-- #main Starts -->
            <?php woo_main_before(); ?>
            <section id="main" class="col-xs-12">
				<?php
					woo_loop_before();
					
					if (have_posts()) { $count = 0;
						while (have_posts()) { the_post(); $count++;
							woo_get_template_part( 'content', 'page' ); // Get the page content template file, contextually.
						}
					}
					
					woo_loop_after();
				?>     
            </section><!-- /#main -->
            <?php woo_main_after(); ?>
    
            <?php get_sidebar(); ?>

            

		</div><!-- /#main-sidebar-container -->         

		<?php get_sidebar( 'alt' ); ?>
			
    </div><!-- /#content -->

	<?php woo_content_after(); ?>
    </div><!--end content-bg-->
<?php get_footer(); ?>