<?php
/**
 * Template Name: Home Page 2
 *
 * Here we setup all logic and XHTML that is required for the index template, used as both the homepage
 * and as a fallback template, if a more appropriate template file doesn't exist for a specific context.
 *
 * @package WooFramework
 * @subpackage Template
 */
 global $woo_options;
?>    
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
	
		<meta charset="<?php echo esc_attr( get_bloginfo( 'charset' ) ); ?>" />
		<title><?php woo_title(); ?></title>

		<?php woo_meta(); ?>
		<link rel="pingback" href="<?php echo esc_url( get_bloginfo( 'pingback_url' ) ); ?>" />
		<?php wp_head(); ?>
		<?php woo_head(); ?>

		<!-- google fonts -->
		<link href='//fonts.googleapis.com/css?family=Overlock:700italic|Open+Sans:800italic,700,400|Open+Sans+Condensed:700|Satisfy' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="/wp-content/themes/mcgonigles/fonts/MyFontsWebfontsKit.css"/>
		<!-- font awesome -->
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<?php if(is_page(43) or is_page(44) or is_page(42) or is_page(41) or is_page(47) or is_page(13) or is_page(45) or is_page(46) or is_page(50) or is_page(123) or is_page(125) or is_page(127) or is_page(128) or is_page(18) or is_page(400) or is_page(75) or is_page(433) or is_page(15) or is_page(73) or is_page(467) or is_page(471) or is_page(857)){ ?>
			<style>
			header { background-color: rgb(255, 255, 255);
    			/* RGBa with 0.6 opacity */
   			background-color: rgba(255, 255, 255, 0.6);
			padding: 0 3.5% 0 3.5%;
			}
			h1 {
			font-family: NexaRustScriptB-03 !important;
			margin-bottom: 0;
			padding: 15px 0 5px 0;
			letter-spacing: 1.2px !important;
			font-size: 36px !important;
			}
			#page {background: url(/wp-content/themes/mcgonigles/img/bg-meat-pages.jpg) repeat-y; background-size: 100%;}
			</style>
    		<?php } ?>
<?php if(is_page(433)){ ?>
			<style>
			h1 {
			text-align: center;
			}
			</style>
    		<?php } ?>
		<?php if(is_page(42) or is_page(41) or is_page(47)){ ?>
			<style>
			.meat-description a {color: #008fe1; background: none; padding: initial; font-size: inherit; font-weight: inherit; margin-top: 0; line-height: inherit;}
			.meat-description a:hover {background: none; color: #9d190e;}
			</style>
    		<?php } ?>
		<?php if(is_page(123) or is_page(125) or is_page(127) or is_page(128) or is_page(400) or is_page(857)){ ?>
			<style>
			h1 {text-align: center;}
			</style>
    		<?php } ?>
		<?php if(is_home()){ ?>
			<style>
			#page {background: url(/wp-content/themes/mcgonigles/img/bg-meat-pages.jpg) repeat-y; background-size: 100%;}
			#main-container {background-color: rgb(255, 255, 255);
   			 /* RGBa with 0.6 opacity */
   			 background-color: rgba(255, 255, 255, 0.6);
			 margin-top: 40px;
			}
			#main-container section#main{padding-top: 15px;}
			#main-container section#secondary {padding-top: 0px !important;}
			</style>
		<?php } ?>
		<?php if(is_page(338)){ ?>
			<style>
			#page {background: url(/wp-content/themes/mcgonigles/img/bg-meat-pages.jpg) repeat-y; background-size: 100%;}
			#main-container {background-color: rgb(255, 255, 255);
   			 /* RGBa with 0.6 opacity */
   			 background-color: rgba(255, 255, 255, 0.6);
			}
			#main-container section#main{padding-top: 15px; margin-top: 70px;}
			#main-container section#secondary {padding-top: 0px !important;}
			</style>
		<?php } ?>
<?php if(is_page(805)){ ?>
<style>
#main-container section#main {padding:80px 15px !important;}
.iphorm-group-title {font-size: 20px;}
.iphorm_2_1-input-wrap div.selector {font-size: 16px !important;}
.iphorm_2_1-element-wrap label {font-weight: 700 !important; font-size: 20px !important;}
.iphorm-element-spacer-select label {float: left; width: 90%; display: block; max-width: 490px;}
.iphorm-element-spacer label {font-weight: 400;}
.iphorm-element-wrap .iphorm-input-wrap-select {float-left: width: 10%;display: block;}
.iphorm-group-wrap .iphorm-group-wrap, .iphorm_2_146-group-wrap, .iphorm_2_140-group-wrap, .iphorm_2_57-group-wrap, .iphorm_2_65-group-wrap, .iphorm_2_1-element-wrap {background: #d0d0d0; border-radius: 8px; padding: 10px; max-width: 570px; color: #000 !important;}
.iphorm_2_1-element-wrap {max-width: 680px;}
.iphorm-group-row p {color: #000 !important;}
.iphorm_2_128-outer-label, .iphorm_2_129-outer-label {font-size: 18px !important; font-weight: 700 !important;}
.iphorm_2_162-element-wrap p {font-size: 13px !important; font-weight: 700 !important;}
.iphorm_2_88-group-wrap {width: 100%; max-width: 570px;}
.iphorm_2_88-group-wrap input, .iphorm_2_88-group-wrap textarea {width: 100% !important;}
.tf-label {width: 100%; clear: both; font-weight: 700; overflow: hidden; margin-bottom: 10px;}
.tf-label .label {float: left; width: 90%; display: block; max-width: 490px; font-size: 18px;}
.tf-label .qty {float: left; width: 10%; display: block;}
</style>
<?php } ?>
		<?php if(is_page(16)){ ?>
			<style>
			#main-content {display: none;}
			#section-footer-buckets {background: none;}
			#footer-top {background: url(/wp-content/themes/mcgonigles/img/bg-meat-pages.jpg) repeat-y; background-size: 100%; overflow: hidden;}
			#section-footer-banner {border-bottom: 0; margin-bottom: 30px;}
			</style>
    		<?php } ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-37523351-1', 'auto');
  ga('send', 'pageview');

</script>
<style type="text/css">
			#slider .metaslider .caption-wrap, .meat-description h2, #fresh-seafood-list h2 {font-family:NexaRustScriptB-03 !important;}
			.reset_variations {display: none !important;}
			header#header #user-actions ul {font-size: 15px; padding: 5px 0;}
			.list-inline > li:last-child {padding-right: 0; !important}
			.cart_totals .shipping {display: none;}
			#e_deliverydate_field small:first-of-type {display: none !important;}
			#e_deliverydate_field br:first-of-type {display: none;}
			#e_deliverydate_field small {font-weight: 700;}
			#e_deliverydate_field small br {display: initial !important;}
			#e_deliverydate_field small strong {color: #d11f29;}
			#address_form .h2-link {background-color: #e6e6e6; color: #000; font-weight: 700; font-size: 16px; padding: 3px 10px; margin-top: 10px; display: block; max-width: 244px;}
			#address_form .h2-link:hover {background-color: #da2c35; text-decoration: none; color: #fff;}
 .custom_field_woo input { 
background-color:#FFFFFF;
 background-image:none;
  border:1px solid #CCCCCC;
  border-radius:4px;
  box-shadow:rgba(0, 0, 0, 0.0745098) 0 1px 1px inset;
  color:#555555;
  display:block;
  font-size:14px;
  height:34px;
  line-height:1.42857;
  padding:6px 12px;
  transition:border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  width:100%;
}

#ui-datepicker-div {padding: 5px 10px; background: #fff; border: 1px solid #000;}
.ui-datepicker-prev {padding: 0 10px 0 0;}
.green-button {display: block; float: right; background-color: #5cb85c; padding: 7px 15px; color: #fff; clear: both; border-radius: 10px; margin-bottom: 15px;}
.green-button:hover {color: #fff; text-decoration: none;}
#main-container section#main {padding: 75px 15px;}
input#createaccount {float: left; width: 20px;}
#ship-to-different-address {overflow: hidden; clear: both;}
#ship-to-different-address label {width: auto; float: left; margin: 0 5px 10px 0;}
#ship-to-different-address input {width: 18px; height: 18px; float: left;}
.seafood-list-item #price.table-list-3 {text-align: right;}

@media screen and (max-width: 700px) {
.product_list_widget li {width: 50%;}
.woocommerce .summary table.variations tr td {display: block;}
.create-account {clear: both; overflow: hidden; display: block; float: none;}
.seafood-list-item .table-list-3 {width: 100%;}
.seafood-list-item .table-list-3:last-child {width: 100%;}
}
#main .page-description {overflow: hidden !important;}

@media screen and (max-width: 500px) {
.woocommerce .summary table.variations tr td {display: block;}
.product_list_widget li {width: 100%;}
.woocommerce .summary form {padding: 0 !important; overflow: hidden;}
}

header#header #user-actions ul {width: 180px !important;}
header#header #user-actions li {width: 100%;}
.login-page .table-list-3 {padding: 0 3% 15px 3%;}
.login-page .gray {background-color: #eeeeee;}
.login-page h2 {font-size: 36px; text-align: center;}
.login-page h3 {text-align: center; color: #000; font-size: 28px;}
		</style>

<script type='text/javascript'>
window.__lo_site_id = 76007;

	(function() {
		var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
		wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
	  })();
	</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '228530874239342'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=228530874239342&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

	</head>
	<body <?php body_class(); ?>>
	<?php woo_top(); ?>

	<!-- HEADER -->
	<?php // woo_header_before(); ?>

	<header id="header" class="new-header">
		<div id="header-container">
			<div id="header-content">
				
				<!-- logo - left column -->
				<div id="logo">
					<a href="https://www.mcgonigles.com/"><img src="/wp-content/themes/mcgonigles/img/logo.png" alt="mcgonigles market local kansas city meat market since 1951" title="McGonigle's Market a Kansas City Tradition Since 1951"></a>
				</div>
				<div id="mobile-headline" class="hidden-xs">
					<span>Must Be<br/>McGonigle's...</span>
					<p>A Kansas City Tradition Since 1951</p>
				</div><!--end mobile-headline-->
				<!-- nav - right column -->
				<div id="nav-primary">
					<div id="header-phone">
						Store Phone:<br/> <strong>(816) 444-4720</strong><br/>
						Shipping Phone:<br/> <strong>1 (888) 783-2540</strong>
					</div><!--end header-phone-->
					<!-- row: sign in -->
					<div id="user-actions-content">
						<div id="user-actions">
							<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("User Actions") ) : ?><?php endif; ?>
						</div>
					</div>

					<!-- row: nav -->
					<div id="nav-content">
						
						<nav class="navbar navbar-default">
							
							<!-- Brand and toggle get grouped for better mobile display -->
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar">Menu</span>
								</button>
								<a class="location" href="https://www.google.com/maps/place/McGonigle's+Market/@38.9852554,-94.6080492,17z/data=!3m1!4b1!4m5!3m4!1s0x87c0eed5d13b2697:0x9532afc7d4596a32!8m2!3d38.9852513!4d-94.6058605" target="_blank">Location</a>
								<a class="call-now" href="tel:555-555-5555" target="_blank"><img src="/wp-content/themes/mcgonigles/img/call-now.png" /></a>
							</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<?php wp_nav_menu( array('menu' => 'Primary Navigation' )); ?>
							</div><!-- /.navbar-collapse -->
							
						</nav>

					</div>

					<!-- row: tagline -->
					<div id="tagline-content" class="hidden-xs">
						
						<h3 id="tagline" class="new">For Quality Steaks and More, It Must Be McGonigle's...A Kansas City Tradition Since 1951</h3>
						
					</div>
				</div>
			</div>
		</div>
	</header>

	<?php // woo_header_after(); ?>
	<!-- // HEADER -->

	<!-- SHOPPING PAGES ONLY: FEATURED IMAGE --> 
    <div id="shopping-featured-image">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Woocommerce Feature Images") ) : ?><?php endif; ?>
    </div>       
    <!-- // SHOPPING PAGES ONLY: FEATURED IMAGE -->

	<!-- CART -->
    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Cart") ) : ?><?php endif; ?>      
    <!-- // CART -->

	<div id="page">	
  
    
    <!-- SLIDER -->    
    <div id="slider" class="hidden-xs">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Slider") ) : ?><?php endif; ?>
    </div>
    <!-- // SLIDER -->

    <!-- FUNNEL -->
    <div id="section-funnel" class="hidden-xs">
        <div id="funnel-container">
            <div id="funnel-content">                
                <ul><li id="fnl-market"><a class="hvr-float-shadow" href="/local-market-kansas-city/" title="McGonigle's Local Market" alt="local meat market in kansas city">Our Market</a></li><li id="fnl-online"><a class="hvr-float-shadow" href="/shop/" title="Shop Online For the Finest Meats" alt="mail order steak shipping">Ship Steaks</a></li><li id="fnl-to-go"><a class="hvr-float-shadow" href="/local-market-kansas-city/kansas-city-bbq-equipment-supplies/" title="McGonigle's BBQ" alt="BBQ To Go Kansas city" >BBQ To Go</a></li><li id="fnl-cater"><a class="hvr-float-shadow" href="/local-market-kansas-city/catering-kansas-city/" title="Local Kansas City Catering" alt="catering company located in kansas city">Catering</a></li><li id="fnl-cards"><a class="hvr-float-shadow" href="/gift-ideas/gift-cards/" title="Order McGonigle's Gift Cards" alt="mcgonigles meat market gift card order">Gift Cards</a></li></ul>                
            </div>
        </div>
    </div>
    <!-- // FUNNEL -->
	<div id="funnel-mobile">
		<ul>
			<li id="fm-local-market">
				<a href="/local-market-kansas-city/"></a>
			</li>
			<li id="fm-weekly-specials">
				<a href="/weekly-specials/"></a>
			</li>
			<li id="fm-steak-shipping">
				<a href="/shop/"></a>
			</li>
			<li id="fm-bbq">
				<a href="/local-market-kansas-city/kansas-city-bbq-equipment-supplies/"></a>
			</li>
			<li id="fm-catering">
				<a href="/local-market-kansas-city/catering-kansas-city/"></a>
			</li>
			<li id="fm-gift-cards">
				<a href="/gift-ideas/gift-cards/"></a>
			</li>
		</ul>
	</div><!--end funnel-mobile-->
    <!-- HOME SPECIALS -->
    <div class="row hidden-xs" id="home-specials">
    	<div class="inner">
    		 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Home - Specials") ) : ?><?php endif; ?>
    	</div><!--end inner-->
    </div><!--end home-specials-->
	<!-- // HOME SPECIALS -->

 <!-- FINE MEETS -->
  <div class="row hidden-xs" id="home-steak-shipping">
  		<div class="inner">
  			<div id="logo-callout" class="col-xs-12 col-sm-4">
                    <img src="/wp-content/themes/mcgonigles/img/logo-fine-meats.png" title="Providing The Finest Meats to Kansas City Since 1951" alt="Fine Meats logo" />
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Fine Meats -  Callout") ) : ?><?php endif; ?>
  			</div><!--end logo-callout-->
  			<div id="steak" class="col-xs-12 col-sm-8">
  				 <img src="/wp-content/themes/mcgonigles/img/steak.png" alt="Steak Image" />
  			</div><!--end steak-->
  		</div><!--end inner-->
  </div><!--end row-->
<!-- // FINE MEETS -->
<!-- FRESH CATCH -->
   <div id="home-fresh-catch" class="row hidden-xs">
	<div class="inner">
		<div class="col-xs-hidden col-sm-3"></div>
		<div class="col-xs-12 col-sm-5" id="fresh-catch-details">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Fresh Catch - Copy") ) : ?><?php endif; ?>
		</div>
		<div class="col-xs-12 col-sm-4" id="fresh-catch-logo">
			 <img src="/wp-content/themes/mcgonigles/img/logo-fresh-catch.png" title="Fresh Catch of the Day" alt="fresh seafood fish local kansas city market" />
             <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Fresh Catch - Callout") ) : ?><?php endif; ?>
		</div>
	</div><!--end inner-->
</div>
<!-- // FRESH CATCH -->

    <!-- BBQ TO GO -->
    <div id="section-bbg-to-go" class="hidden-xs">
        <div id="bbq-to-go-container">
            <div id="bbq-to-go-content">
            	<div id="bbq-to-go-logo">
                    <img src="/wp-content/themes/mcgonigles/img/logo-bbq-to-go.png" title="BBQ To Go at McGonigle's" alt="BBQ To Go kansas city area" />
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("BBQ - Callout") ) : ?><?php endif; ?>
                </div>
                <div id="burger" class="hidden-xs">
                    <img src="/wp-content/themes/mcgonigles/img/pic-burger-ko.png" alt="Burnt End Sandwich" />
                </div>
            </div>
        </div>
    </div>
<!-- // BBQ TO GO -->
<!-- CHOICE REWARDS -->
<div id="choice-rewards-section" class="row hidden-xs">
	<div class="inner">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Choice Rewards") ) : ?><?php endif; ?>
	</div><!--end inner-->
</div><!--end choice-rewards-section-->
<!-- // CHOICE REWARDS -->
    <!-- TIMELINE -->
    <div id="section-timeline" class="hidden-xs">
        <div id="timeline-liner">
            <div id="timeline-container">
                <div id="timeline-content">
                    <div id="timeline">
                        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Timeline") ) : ?><?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- // TIMELINE -->    
     <!-- MIKE'S BLOG -->
    <div id="section-blog" class="hidden-xs">
        <div id="blog-container">
            <div id="blog-content">
                <div id="blog-items">
                    <h2>Mike's Blog</h2>
                    <img class="hidden-md hidden-lg" src="/wp-content/themes/mcgonigles/img/logo-mikes-blog.png" title="Check Out Mike's Blog for the Latest News, Events and Special Offers" alt="Mike's Blog news tips specials" />
                    <ul>
                        <?php $the_query = new WP_Query( 'showposts=3' ); ?>

                        <?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
                        <li>

                            <!-- month / day utilities, in case you need them -->
                            <!-- <div class="month"><?php // echo mysql2date('M', $post->post_date) ?></div> -->
                            <!-- <div class="day"><?php // echo mysql2date('d', $post->post_date) ?></div> -->
                            

                            <div class="media">
                                <?php if ( has_post_thumbnail() ): ?>
                                <div class="media-left">
                                    <a href="<?php the_permalink() ?>">                                  
                                       <?php the_post_thumbnail( 'thumbnail' ); ?>
                                    </a>
                                </div>
                                <?php endif; ?>
                                <div class="media-body">
                                    <h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                                    <?php the_excerpt(); ?>
                                    <a class="link-read-more" href="<?php the_permalink() ?>">                                  
                                       [ READ MORE ]
                                    </a>
                                </div>
                            </div>

                        </li>
                        <?php endwhile;?>
                    </ul>
                </div>
                <div id="logo-callout" class="hidden-xs hidden-sm">
                    <img src="/wp-content/themes/mcgonigles/img/logo-mikes-blog.png" alt="Mike's Blog logo" />
                </div>
            </div>
        </div>
    </div>
    <!-- // MIKE'S BLOG -->
<div id="footer-top">
    <!-- FOOTER BUCKETS -->
    <div id="section-footer-buckets" class="hidden-xs">
	<?php if(is_page(16)){ ?>
        <div class="container-fluid">
            <div id="footer-buckets-content">
            	<!-- gift ideas-->
                <div id="gift-ideas">
                    <div class="row">
                    	<div class="col-xs-12 col-md-7">
                    		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Top - Gift Ideas") ) : ?><?php endif; ?>
                    	</div>
                    	<div class="hidden-xs hidden-sm col-md-5">
                    		<img src="/wp-content/themes/mcgonigles/img/pic-gift-ideas.jpg" alt="Gift Ideas illustration" />
                    	</div>
                    </div>
                </div>
                <!-- // gift ideas -->
                <!-- in-store packages -->
				<div id="in-store-packages">
                    <div class="row">
                    	<div class="col-xs-12 col-md-7">
                    		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Top - In Store Packages") ) : ?><?php endif; ?>
                    	</div>
                    	<div class="hidden-xs hidden-sm col-md-5">
                    		<img src="/wp-content/themes/mcgonigles/img/pic-in-store-packages.jpg" alt="image of In-Store Packages" />
                    	</div>
                    </div>
                </div>
                <!-- // in-store packages -->
            </div>
        </div>
	<?php } ?>
<div class="container-fluid">
            <div id="footer-buckets-content">
            	<!-- steak shipping -->
                <div id="steak-shipping">
                    <div class="row">
                    	<div class="col-xs-12 col-md-6">
                    		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Top - Steak Shipping") ) : ?><?php endif; ?>
                    	</div>
                    	<div class="hidden-xs hidden-sm col-md-6">
                    		<img src="/wp-content/themes/mcgonigles/img/pic-steak-shipping.png" title="Mail Order Steak Shipping of the Finest Cuts of Meat" alt="nation wide steak shipping top quality meat cuts" />
                    	</div>
                    </div>
                </div>
                <!-- // steak shipping -->
                <!-- gift cards -->
				<div id="gift-cards">
                    <div class="row">
                    	<div class="col-xs-12 col-md-6">
                    		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Top - Gift Cards") ) : ?><?php endif; ?>
                    	</div>
                    	<div class="hidden-xs hidden-sm col-md-6">
                    		<img src="/wp-content/themes/mcgonigles/img/pic-gift-cards.jpg" title="A McGonigle's Gift Card Makes the Perfect Gift" alt="mcgonigles market kansas city gift card" />
                    	</div>
                    </div>
                </div>
                <!-- // gift cards -->
            </div>
        </div>
    </div>
    <!-- // FOOTER BUCKETS -->
</div><!--// FOOTER TOP -->
    <!-- FOOTER -->
   <footer id="footer" class="row new">
	<div class="inner">
		<div id="footer-left" class="col-xs-12 col-sm-3">
			<a href="/"><img src="/wp-content/themes/mcgonigles/img/logo.png" title="McGonigle's Market Serving Kansas City Since 1951" alt="MmcGonigles market kansas city local meat market"></a>
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer - Left") ) : ?><?php endif; ?>
		</div><!--end footer-left-->
		<div id="footer-right" class="col-xs-12 col-sm-9">
			<h2>Located at the Corner of Ward Pkwy & 79th St.</h2>
			<div id="map-hours">
				<div id="map" class="col-xs-12 col-sm-8">
					<div class="inner">

					</div><!--end inner-->
				</div><!--end map-->
				<div id="hours" class="col-xs-12 col-sm-4">
					<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer - Hours") ) : ?><?php endif; ?>
				</div><!--end hours-->
			</div><!--end map-hours-->
		</div><!--end footer-right-->
	</div><!--end inner-->
	<div id="overlay-container">
		<div id="footer-social" class="col-xs-12">
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer - Social") ) : ?><?php endif; ?>
			</div><!--end footer-social-->
		<div id="copyright" class="col-xs-12 centered">
			<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Footer Menu") ) : ?><?php endif; ?>
        	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Copyright & Bug") ) : ?><?php endif; ?>
		</div><!--end copyright-->
	</div><!--end overlay-container-->
</footer>
    <!-- // FOOTER -->

	<?php wp_footer(); ?>
	<script type='text/javascript' src='https://www.mcgonigles.com/wp-content/themes/mcgonigles/js/slick.min.js'></script>
	<script type='text/javascript' src='https://www.mcgonigles.com/wp-content/themes/mcgonigles/js/jquery.waypoints.min.js'></script>
	<script type='text/javascript' src='https://www.mcgonigles.com/wp-content/themes/mcgonigles/js/app2.js'></script>
	<script>
// header: off top scroll behavior 
jQuery.noConflict();
jQuery(document).ready(function( $ ) {

	// set variables	
	var $document = $(document),
    $element = $('header#header'),
    className = 'offtop';

	$document.scroll(function() {
	  $element.toggleClass(className, $document.scrollTop() >= 50);
	});
	
});
jQuery.noConflict();
jQuery(document).ready(function( $ ) {

	// set variables	
	var $document = $(document),
    $element = $('div#page'),
    className = 'offtop';

	$document.scroll(function() {
	  $element.toggleClass(className, $document.scrollTop() >= 50);
	});
	
});
</script>
</body>
</html>