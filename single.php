<?php
/**
 * Single Post Template
 *
 * This template is the default page template. It is used to display content when someone is viewing a
 * singular view of a post ('post' post_type).
 * @link http://codex.wordpress.org/Post_Types#Post
 *
 * @package WooFramework
 * @subpackage Template
 */

get_header();
?>

	<!-- single.php -->

	<!-- SECONDARY NAV -->
	<!-- The structure for this is in functions.php where the widget is registered... -->
	<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Secondary Menus") ) : ?><?php endif; ?>	

	<!-- SLIDER -->    
    <div id="slider" class="slider-secondary">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Slider") ) : ?><?php endif; ?>
    </div>
    <!-- // SLIDER -->

       
    <!-- #content Starts -->
	<?php woo_content_before(); ?>
    
    
    	<div id="main-container">    

            <div id="main-content">
            	
            	<!-- #main Starts -->
	            <?php woo_main_before(); ?>

	            <section id="main">        

					<h2 id="link-return-to-blog"><a href="/mikes-blog">« Return to Mike's Blog</a></h1>

					<?php
						woo_loop_before();
						
						if (have_posts()) { $count = 0;
							while (have_posts()) { the_post(); $count++;
								
								woo_get_template_part( 'content', get_post_type() ); // Get the post content template file, contextually.
							}
						}
						
						woo_loop_after();
					?> 

	            </section><!-- /#main -->	            
	            <?php woo_main_after(); ?>

	            <section id="secondary">
                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Blog Sidebar") ) : ?><?php endif; ?>
                </section>

            </div>

		</div><!-- /#main-container -->         

    
	<?php woo_content_after(); ?>

<?php get_footer(); ?>